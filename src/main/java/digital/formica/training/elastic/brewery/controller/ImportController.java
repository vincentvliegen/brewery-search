package digital.formica.training.elastic.brewery.controller;

import digital.formica.training.elastic.brewery.service.ImportService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("import")
public class ImportController {
    private final ImportService importService;
    private final String nameIndex;

    public ImportController(ImportService importService, @Value("${nameIndex}") String nameIndex) {
        this.importService = importService;
        this.nameIndex = nameIndex;
    }

    @PostMapping()
    public ModelAndView importData(){
        return new ModelAndView("dashboard")
                .addObject("importAmount", importService.importBreweries(nameIndex));
    }
}
