package digital.formica.training.elastic.brewery.service;

public interface ImportService {
    int importBreweries(String nameIndex);
}
