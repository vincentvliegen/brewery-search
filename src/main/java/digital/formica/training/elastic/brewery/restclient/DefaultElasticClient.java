package digital.formica.training.elastic.brewery.restclient;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import digital.formica.training.elastic.brewery.domain.Brewery;
import org.apache.http.HttpHost;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.*;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.BucketOrder;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class DefaultElasticClient implements ElasticClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultElasticClient.class);


    private final RestHighLevelClient client;
    private final String mappingLocation;
    private final String settingsLocation;
    private final static String PRETAG = "<b><em>";
    private final static String POSTTAG = "</em></b>";


    public DefaultElasticClient(@Value("${mappingLocation}") String mappingLocation, @Value("${settingsLocation}") String settingsLocation) {
        HttpHost host = new HttpHost("localhost", 9200, "http");
        RestClientBuilder restClientBuilder = RestClient.builder(host);
        this.client = new RestHighLevelClient(restClientBuilder);
        this.mappingLocation = mappingLocation;
        this.settingsLocation = settingsLocation;
    }

    @Override
    public IndicesClient getIndicesClient() {
        return client.indices();
    }

    @Override
    public void createIndex(String nameIndex) {
        try {
            if (indexIsPresent(nameIndex)) {
                deleteIndex(nameIndex);
            }
            CreateIndexRequest createIndexRequest = new CreateIndexRequest(nameIndex);
            createIndexRequest.mapping(getConfigurationFromFile(mappingLocation), XContentType.JSON);
            createIndexRequest.settings(getConfigurationFromFile(settingsLocation), XContentType.JSON);
            getIndicesClient().create(createIndexRequest, RequestOptions.DEFAULT);
        } catch (IOException e) {
            String message = "Failed to create index.";
            LOGGER.error(message, e);
        }
    }

    @Override
    public void deleteIndex(String nameIndex) {
        DeleteIndexRequest deleteIndexRequest = new DeleteIndexRequest(nameIndex);
        try {
            getIndicesClient().delete(deleteIndexRequest, RequestOptions.DEFAULT);
        } catch (IOException e) {
            String message = "Failed to delete index.";
            LOGGER.error(message, e);
        }
    }

    @Override
    public boolean indexIsPresent(String nameIndex) throws IOException {
        GetIndexRequest getIndexRequest = new GetIndexRequest(nameIndex);
        return getIndicesClient().exists(getIndexRequest, RequestOptions.DEFAULT);
    }

    @Override
    public int bulkUpdate(List<Brewery> allBreweries, String nameIndex) {
        BulkRequest bulkRequest = new BulkRequest();
        ObjectMapper objectMapper = new ObjectMapper();
        int failed = 0;
        for (Brewery brewery : allBreweries) {
            try {
                IndexRequest indexRequest = new IndexRequest(nameIndex);
                indexRequest.source(objectMapper.writeValueAsString(brewery), XContentType.JSON);
                bulkRequest.add(indexRequest);
            } catch (JsonProcessingException e) {
                failed++;
                String message = "Failed to convert brewery to JSON String.";
                LOGGER.error(message, e);
            }
        }
        try {
            client.bulk(bulkRequest, RequestOptions.DEFAULT);
            return allBreweries.size() - failed;
        } catch (IOException e) {
            String message = "Failed to bulk update breweries to index.";
            LOGGER.error(message, e);
            return 0;
        }
    }

    @Override
    public SearchSourceBuilder createSearchSource(String query, int from, int size, List<String> provinces){
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        if (query.isEmpty()) {
            searchSourceBuilder.query(QueryBuilders.matchAllQuery());
        } else {
            searchSourceBuilder.query(QueryBuilders.queryStringQuery(query));
            searchSourceBuilder.highlighter(createDescriptionHighLightBuilder());
        }
        if (provinces != null) searchSourceBuilder.postFilter(QueryBuilders.termsQuery("province", provinces));
        searchSourceBuilder.aggregation(createProvinceAggregation());
        searchSourceBuilder.from(from);
        searchSourceBuilder.size(size);
        return searchSourceBuilder;
    }

    @Override
    public SearchRequest createSearchRequest(String nameIndex, SearchSourceBuilder searchSourceBuilder){
        SearchRequest searchRequest = new SearchRequest(nameIndex);
        searchRequest.source(searchSourceBuilder);
        return searchRequest;
    }

    @Override
    public SearchResponse getSearchResponse(SearchRequest searchRequest){
        try {
            return client.search(searchRequest, RequestOptions.DEFAULT);
        } catch (IOException e) {
            String message = "Failed to search.";
            LOGGER.error(message, e);
            return null;
        }
    }

    @Override
    public List<Brewery> getBreweriesFromResponse(SearchResponse searchResponse) {
        List<Brewery> listBreweries = new ArrayList<>();
        ObjectMapper objectMapper = new ObjectMapper();
        for (SearchHit searchHit : searchResponse.getHits().getHits()) {
            String jsonString = searchHit.getSourceAsString();
            try {
                Brewery brewery = objectMapper.readValue(jsonString, Brewery.class);
                if(!searchHit.getHighlightFields().isEmpty()) {
                    brewery.setDescription(getDescriptionWithHighlighting(searchHit));
                }
                listBreweries.add(brewery);
            } catch (IOException e) {
                String message = "Failed to read brewery";
                LOGGER.error(message, e);
            }
        }
        return listBreweries;
    }

    @Override
    public Map<String, Integer> getAggregationsOnProvince(SearchResponse searchResponse){
        Map<String, Integer> map = new HashMap<>();
        Terms provinces = searchResponse.getAggregations().get("provinces");
        for(Terms.Bucket province: provinces.getBuckets()){
            map.put(province.getKeyAsString(), (int) province.getDocCount());
        }
        return map;
    }

    private String getConfigurationFromFile(String path) throws IOException {
        return new String(Files.readAllBytes(new ClassPathResource(path).getFile().toPath()));
    }

    private HighlightBuilder createDescriptionHighLightBuilder() {
        HighlightBuilder highlightBuilder = new HighlightBuilder();
        HighlightBuilder.Field description = new HighlightBuilder.Field("description");
        highlightBuilder.field(description);
        highlightBuilder.numOfFragments(0);
        highlightBuilder.preTags(PRETAG);
        highlightBuilder.postTags(POSTTAG);
        return highlightBuilder;
    }

    private AggregationBuilder createProvinceAggregation(){
        return AggregationBuilders.terms("provinces").field("province").order(BucketOrder.key(true));
    }

    private String getDescriptionWithHighlighting(SearchHit searchHit){
        Text[] fragments = searchHit.getHighlightFields().get("description").getFragments();
        StringBuilder stringBuilder = new StringBuilder();
        for (Text fragment : fragments) {
            stringBuilder.append(fragment.toString());
        }
        return stringBuilder.toString();
    }

}
