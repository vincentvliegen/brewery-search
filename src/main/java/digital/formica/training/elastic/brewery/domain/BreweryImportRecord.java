package digital.formica.training.elastic.brewery.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BreweryImportRecord {

    @JsonProperty("Name")
    private String name;
    private String street;
    @JsonProperty("house_number")
    private String houseNumber;
    @JsonProperty("box_number")
    private String boxNumber;
    @JsonProperty("postal_ code")
    private String postalCode;
    @JsonProperty("city_name")
    private String city;
    private String province;
    @JsonProperty("description_nl")
    private String description;
    @JsonProperty("Website")
    private String website;

    public String getName() {
        return name;
    }

    public String getStreet() {
        return street;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public String getBoxNumber() {
        return boxNumber;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getCity() {
        return city;
    }

    public String getProvince() {
        return province;
    }

    public String getDescription() {
        return description;
    }

    public String getWebsite() {
        return website;
    }
}
