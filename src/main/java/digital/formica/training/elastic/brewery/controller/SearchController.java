package digital.formica.training.elastic.brewery.controller;

import digital.formica.training.elastic.brewery.service.ElasticService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping("search")
public class SearchController {
    private final ElasticService elasticService;
    private final String nameIndex;

    public SearchController(ElasticService elasticService, @Value("${nameIndex}") String nameIndex) {
        this.elasticService = elasticService;
        this.nameIndex = nameIndex;
    }

    @GetMapping()
    public ModelAndView showSearchResults(@RequestParam String query,
                                          @RequestParam(defaultValue = "1") int pageNumber,
                                          @RequestParam(required = false) List<String> checkBox) {
        return new ModelAndView("dashboard")
                .addObject("searchResult", elasticService.searchBreweries(nameIndex, query, pageNumber,checkBox))
                .addObject("query", query);
    }
}
