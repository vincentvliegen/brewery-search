package digital.formica.training.elastic.brewery.domain;

import org.elasticsearch.common.unit.TimeValue;

import java.util.List;

public class SearchResult {

    private final List<Brewery> breweries;
    private final TimeValue searchTime;
    private final int searchCount;
    private final int pageNumber;
    private final int pageCount;
    private final List<Province> provinces;

    public SearchResult(List<Brewery> breweries, TimeValue searchTime, int searchCount, int pageNumber, int pageCount, List<Province> provinces) {
        this.breweries = breweries;
        this.searchTime = searchTime;
        this.searchCount = searchCount;
        this.pageNumber = pageNumber;
        this.pageCount = pageCount;
        this.provinces = provinces;
    }

    public List<Brewery> getBreweries() {
        return breweries;
    }

    public TimeValue getSearchTime() {
        return searchTime;
    }

    public int getSearchCount() {
        return searchCount;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public int getPageCount() {
        return pageCount;
    }

    public List<Province> getProvinces() {
        return provinces;
    }
}
