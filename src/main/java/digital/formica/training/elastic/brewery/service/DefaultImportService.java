package digital.formica.training.elastic.brewery.service;

import digital.formica.training.elastic.brewery.domain.Brewery;
import digital.formica.training.elastic.brewery.domain.BreweryImportRecord;
import digital.formica.training.elastic.brewery.restclient.ElasticClient;
import digital.formica.training.elastic.brewery.restclient.OpenDataVlaanderenBreweriesClient;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DefaultImportService implements ImportService {

    private final OpenDataVlaanderenBreweriesClient openDataVlaanderenBreweriesClient;
    private final ElasticClient elasticClient;

    public DefaultImportService(OpenDataVlaanderenBreweriesClient openDataVlaanderenBreweriesClient, ElasticClient elasticClient) {
        this.openDataVlaanderenBreweriesClient = openDataVlaanderenBreweriesClient;
        this.elasticClient = elasticClient;
    }

    @Override
    public int importBreweries(String nameIndex) {
        elasticClient.createIndex("breweries");
        return elasticClient.bulkUpdate(listBreweryImportRecordsToListBreweries(openDataVlaanderenBreweriesClient.getBreweries()), nameIndex);
    }


    private List<Brewery> listBreweryImportRecordsToListBreweries(List<BreweryImportRecord> list) {
        return list.stream()
                .map(this::breweryImportRecordToBrewery)
                .collect(Collectors.toList());
    }

    private Brewery breweryImportRecordToBrewery(BreweryImportRecord record) {
        return new Brewery(record.getName(), record.getStreet(), record.getHouseNumber(),
                record.getBoxNumber(), record.getPostalCode(), record.getCity(),
                record.getProvince(), record.getDescription(), record.getWebsite());
    }

}
