package digital.formica.training.elastic.brewery.domain;

public class Brewery {

    private String name;
    private String street;
    private String houseNumber;
    private String boxNumber;
    private String postalCode;
    private String city;
    private String province;
    private String description;
    private String website;

    public Brewery(String name, String street, String houseNumber, String boxNumber, String postalCode, String city, String province, String description, String website) {
        setName(name);
        setStreet(street);
        setHouseNumber(houseNumber);
        setBoxNumber(boxNumber);
        setPostalCode(postalCode);
        setCity(city);
        setProvince(province);
        setDescription(description);
        setWebsite(website);
    }



    public Brewery(){}

    public String getName() {
        return name;
    }

    public String getStreet() {
        return street;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public String getBoxNumber() {
        return boxNumber;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getCity() {
        return city;
    }

    public String getProvince() {
        return province;
    }

    public String getDescription() {
        return description;
    }

    public String getWebsite() {
        return website;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public void setBoxNumber(String boxNumber) {
        this.boxNumber = boxNumber;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setWebsite(String website) {
        this.website = website;
    }
}
