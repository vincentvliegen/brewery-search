package digital.formica.training.elastic.brewery.restclient;

import digital.formica.training.elastic.brewery.domain.BreweryImportRecord;

import java.util.List;

public interface OpenDataVlaanderenBreweriesClient {
    public List<BreweryImportRecord> getBreweries();

}
