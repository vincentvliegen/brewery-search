package digital.formica.training.elastic.brewery.restclient;

import digital.formica.training.elastic.brewery.domain.Brewery;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.IndicesClient;
import org.elasticsearch.search.builder.SearchSourceBuilder;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface ElasticClient {
    IndicesClient getIndicesClient();

    void createIndex(String nameIndex);

    void deleteIndex(String nameIndex);

    boolean indexIsPresent(String nameIndex) throws IOException;

    int bulkUpdate(List<Brewery> allBreweries, String nameIndex);

    SearchSourceBuilder createSearchSource(String query, int from, int size, List<String> provinces);

    SearchRequest createSearchRequest(String nameIndex, SearchSourceBuilder searchSourceBuilder);

    SearchResponse getSearchResponse(SearchRequest searchRequest);

    List<Brewery> getBreweriesFromResponse(SearchResponse searchResponse);

    Map<String, Integer> getAggregationsOnProvince(SearchResponse searchResponse);
}
