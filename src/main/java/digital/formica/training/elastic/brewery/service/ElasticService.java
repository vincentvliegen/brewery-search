package digital.formica.training.elastic.brewery.service;

import digital.formica.training.elastic.brewery.domain.SearchResult;

import java.util.List;

public interface ElasticService {

    SearchResult searchBreweries(String nameIndex, String query, int pageNumber, List<String> checkedProvinces);
}
