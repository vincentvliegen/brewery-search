package digital.formica.training.elastic.brewery.restclient;

import digital.formica.training.elastic.brewery.domain.BreweryImportRecord;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Component
public class DefaultOpenDataVlaanderenBreweriesClient implements OpenDataVlaanderenBreweriesClient {

    private final String dataUrl;

    public DefaultOpenDataVlaanderenBreweriesClient(@Value("${UrlDataBreweries}")String dataUrl) {
        this.dataUrl = dataUrl;
    }

    @Override
    public List<BreweryImportRecord> getBreweries() {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<List<BreweryImportRecord>> response = restTemplate.exchange(dataUrl, HttpMethod.GET, null, new ParameterizedTypeReference<List<BreweryImportRecord>>() {});
        return response.getBody();
    }
}
