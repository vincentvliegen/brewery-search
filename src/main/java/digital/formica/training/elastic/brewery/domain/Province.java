package digital.formica.training.elastic.brewery.domain;

public class Province {

    private final String name;
    private final int count;
    private final boolean checked;

    public Province(String name, int count, boolean checked) {
        this.name = name;
        this.count = count;
        this.checked = checked;
    }

    public String getName() {
        return name;
    }

    public int getCount() {
        return count;
    }

    public boolean isChecked() {
        return checked;
    }
}
