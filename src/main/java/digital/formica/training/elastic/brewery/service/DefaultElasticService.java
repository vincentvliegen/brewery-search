package digital.formica.training.elastic.brewery.service;

import digital.formica.training.elastic.brewery.domain.Province;
import digital.formica.training.elastic.brewery.domain.SearchResult;
import digital.formica.training.elastic.brewery.restclient.ElasticClient;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class DefaultElasticService implements ElasticService {

    private final ElasticClient elasticClient;
    private final static int PAGESIZE = 10;

    public DefaultElasticService(ElasticClient elasticClient) {
        this.elasticClient = elasticClient;
    }

    @Override
    public SearchResult searchBreweries(String nameIndex, String query, int pageNumber, List<String> checkedProvinces) {
        SearchResponse searchResponse = getSearchResponse(nameIndex, query, pageNumber, checkedProvinces);
        int totalFound = (int) searchResponse.getHits().getTotalHits().value;
        int pageCount = totalFound / PAGESIZE + 1;
        int correctedPageNumber = Math.min(pageNumber, pageCount);
        if(correctedPageNumber != pageNumber) searchResponse = getSearchResponse(nameIndex, query, correctedPageNumber, checkedProvinces);
        return new SearchResult(elasticClient.getBreweriesFromResponse(searchResponse),
                searchResponse.getTook(),
                totalFound,
                correctedPageNumber,
                pageCount,
                createListProvinces(elasticClient.getAggregationsOnProvince(searchResponse), checkedProvinces));
    }

    private SearchResponse getSearchResponse(String nameIndex, String query, int pageNumber, List<String> provinces) {
        SearchSourceBuilder searchSourceBuilder = elasticClient.createSearchSource(query, (pageNumber - 1) * PAGESIZE, PAGESIZE, provinces);
        SearchRequest searchRequest = elasticClient.createSearchRequest(nameIndex, searchSourceBuilder);
        return elasticClient.getSearchResponse(searchRequest);
    }

    private List<Province> createListProvinces(Map<String, Integer> namesAndCounts, List<String> checkedProvinces) {
        return namesAndCounts.keySet()
                .stream()
                .map(name -> new Province(name, namesAndCounts.get(name), checkedProvinces == null || checkedProvinces.contains(name)))
                .collect(Collectors.toList());
    }
}
