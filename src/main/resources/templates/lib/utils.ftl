<#macro pageNav>
    <div class="alert alert-secondary" role="alert">
        <nav>
            <ul class="pagination pagination-lg">
                <#list 1..searchResult.pageCount as page>
                    <#if searchResult.pageNumber == page>
                        <li class="page-item active" aria-current="page">
                            <span class="page-link" value=${page}>${page}</span>
                        </li>
                    <#else>
                        <li class="page-item">
                            <button class="page-link" name="pageNumber" value=${page}>${page}</button>
                        </li>
                    </#if>
                </#list>
            </ul>
        </nav>
    </div>
</#macro>